package com.wujunshen;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableDiscoveryClient
@EnableTransactionManagement
@MapperScan("com.wujunshen.dao")
@Slf4j
public class Provider {
    public static void main(String[] args) {
        log.info("start execute Provider....\n");
        SpringApplication.run(Provider.class, args);
        log.info("end execute Provider....\n");
    }
}